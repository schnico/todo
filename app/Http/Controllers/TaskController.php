<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{
    /**
     * The task repository instance
     * @var TaskRepository
     */
    protected $tasks;
    
    /**
     * Create new controller instance
     * 
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
        $this->tasks = $tasks;
    }
    
    public function index(Request $request){
        return view('tasks.index', [
            'tasks' => $this->tasks->forUser($request->user()),
        ]);
    }
    
    public function store(Request $request){
        $this->validate($request, [
            "name" => "required|max:255",
            ]);
        
        $request->user()->tasks()->create([
            'name' => $request->name,
        ]);
        
        return redirect('/tasks');
    }
}
